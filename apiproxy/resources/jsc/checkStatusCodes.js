var statusCodes = JSON.parse(context.getVariable("compositeResponse.subresponseStatusCodes"));
//print("statusCodes: ", statusCodes);


if(statusCodes.indexOf(400) != -1)
{
    context.setVariable("statusCode", 400);
}
else if(statusCodes.indexOf(404) != -1)
{
    context.setVariable("statusCode", 404);   
}
else if(statusCodes.indexOf(500) != -1)
{
    context.setVariable("statusCode", 500);   
}
else{
    print("No failed status codes found.");
}